<?php
//Import Google libraries
//Installing with composer -> composer require google/apiclient:^2.0
require __DIR__ . '/vendor/autoload.php';

///////////////////////////////////////////////////////////////////////////////////
//Configuration variables

//Google sheet ID
  //!!ID for testing
  //ID can be found in URL of the document
define("SpreadsheetID", '1y0wZuZqI8ZTw8U4-aj0wV5QdUOLEhqubNGrLIn5DFk8');

//Relative path from your file to google api credentials file
define("CredentialsPath", '/credentials.json');


/**
 * GoogleApiExporter
 * Creates connection to google spreasheet and processes data from a webpage
 * Created for https://usmevdoschranky.cz/
 * @author     Jan Michalík <jan.michalik@elearnie.com>
 */
class GoogleApiExporter
{
  private string $SpreadsheetID = "";
  private string $CredentialsPath = "";

  /**
   *
   * GoogleApiExporter constructor
   *
   * @param string $spreadsheetID  ID of target spreadsheet, this id could be found in URL of your file on google drive
   * @param string $credentialsPath  Path to credential file generated at google api platform
 */
  function __construct(string $spreadsheetID, string $credentialsPath) {
      $this->SpreadsheetID = $spreadsheetID;
      $this->CredentialsPath = $credentialsPath;
  }

  /**
   *
   * Creates connector to google spreadsheet
   * Uses $this properties as connectors configuration values
   * @return Google_Service_Sheets
   */
  private function CreateConnector()
  {
    $client = new \Google_Client();
    $client->setApplicationName('Processing request');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig(__DIR__ . $this->CredentialsPath);
    return new Google_Service_Sheets($client);
  }

  /**
   *
   * Writes new institution name to the spreatcheet
   * This method writes into the first page in the spreadcheet (index 0) and name "Adresy  Domovů"
   * @param int $ID identifier of new query
   * @param string $InstitutionName  Name of the institution
   * @param string $InstitutionAddress  Adress of the institution
   * @param string $PSC  Postcode
   * @param string $Region  Region name
   * @param int $PostcardCount How many postcartd shall be send
   * @return null
   */
  public function InsertNewAddress(int $ID, string $InstitutionName, string $InstitutionAddress, string $PSC, string $Region, int $PostcardCount)
  {
    $service = $this->CreateConnector();

    //Name of the mail sheet
    $get_range = "Adresy Domovů";
    $response = $service->spreadsheets_values->get($this->SpreadsheetID, $get_range);
    $originalData = $response->getValues();
    $originalDataCount = count($originalData);

    //Update values (for sheet on index 0)
    $update_range = "A" . ($originalDataCount + 1) . ":G" . ($originalDataCount + 1);

    $values = [[$ID, $InstitutionName, $InstitutionAddress, $PSC, $Region, $PostcardCount, "0/" . $PostcardCount]];

    $body = new Google_Service_Sheets_ValueRange([
        'values' => $values
      ]);

    $params = [
        'valueInputOption' => 'RAW'
    ];

    $update_sheet = $service->spreadsheets_values->append($this->SpreadsheetID, $update_range, $body, $params);
    //End of updating Values

    //Updating formatting (for sheet on index 0)
    //Settings value to align center and background for first column
    $styleRequest =
    [
      new Google_Service_Sheets_Request([
          'repeatCell' =>
          [
            "range" =>
            [
              "startRowIndex" => $originalDataCount,
              "endRowIndex" => $originalDataCount + 1,
              "startColumnIndex" => 0,
              "endColumnIndex" => 7
            ],
            "cell" =>
            [
              "userEnteredFormat" =>
              [
                "horizontalAlignment" => "CENTER"
              ],
            ],

            "fields" => "userEnteredFormat(horizontalAlignment)"
          ]
        ]),

      new Google_Service_Sheets_Request([
            'repeatCell' =>
            [
              "range" =>
              [
                "startRowIndex" => $originalDataCount,
                "endRowIndex" => $originalDataCount + 1,
                "startColumnIndex" => 0,
                "endColumnIndex" => 1
              ],
              "cell" =>
              [
                "userEnteredFormat" =>
                [
                  "backgroundColor" =>
                  [
                    "red" => 0.76,
                    "green" => 0.82,
                    "blue" => 0.75
                  ]
                ],
              ],

              "fields" => "userEnteredFormat(backgroundColor)"
            ]
          ])
    ];

    $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
      'requests' => $styleRequest
    ]);

    $response = $service->spreadsheets->batchUpdate($this->SpreadsheetID, $batchUpdateRequest);
    //End of - Updating formatting
  }

  /**
   *
   * Updates value of sent postcards in "Adresy Domovů"
   * Insert row into "Dobrovolníci"
   * This method assumes that there are no missing rows in "Adresy Domovů"
   *   If there are missing values wrong row will be updated
   * @param int $InstitutionID Identifier of institution in "Adresy Domovů" column ID
   * @param string $InstitutionName  Name of institution to be used in "Dobrovolníci"
   * @param int $PostcardCount  Count of postcards to be added to both sheets
   * @param string $ContributorName  Name of the contributor to be added to "Dobrovolníci"
   * @return null
   */
  public function UpdateSentPostcards(int $InstitutionID, string $InstitutionName, int $PostcardCount, string $ContributorName)
  {
      $service = $this->CreateConnector();

      //Updating sent value in "Adresy Domovů"
      $get_range = "Adresy Domovů";
      $response = $service->spreadsheets_values->get($this->SpreadsheetID, $get_range);
      $originalData = $response->getValues();

      $RowID = -1;

      for ($i = 0; $i < count($originalData); $i++) {
          if($originalData[$i][0] != null)
          {
              if($originalData[$i][0] == $InstitutionID)
              {
                $RowID = $i;
                break;
              }
          }
      }

      $sentCell = explode("/",$originalData[$RowID][6]);
      $totalPostcards = $sentCell[1];
      $basePostcards = $sentCell[0];

      $update_range =  "G" . ($RowID + 1);
      $values = [[($basePostcards + $PostcardCount) . "/" . $totalPostcards]];

      $body = new Google_Service_Sheets_ValueRange([
          'values' => $values
        ]);

      $params = [
          'valueInputOption' => 'RAW'
      ];

      $update_sheet = $service->spreadsheets_values->update($this->SpreadsheetID, $update_range, $body, $params);
      //End of - updating value in "Adresy Domovů"

      //Adding row to "Dobrovolníci"
      $get_range = "Dobrovolníci";
      $response = $service->spreadsheets_values->get($this->SpreadsheetID, $get_range);
      $originalData = $response->getValues();
      $originalDataCount = count($originalData);

      $update_range = "Dobrovolníci!A" . ($originalDataCount + 1) . ":D" . ($originalDataCount + 1);

      $values = [[$ContributorName, $InstitutionName, $PostcardCount, date("d/m/Y v H:i")]];

      $body = new Google_Service_Sheets_ValueRange([
          'values' => $values
        ]);

      $params = [
          'valueInputOption' => 'RAW'
      ];

      $update_sheet = $service->spreadsheets_values->append($this->SpreadsheetID, $update_range, $body, $params);
      //End of - Adding row to "Drobovolníci"
  }
}

//Exampeles of use:
  //  $apiWorker = new GoogleApiExporter(SpreadsheetID, CredentialsPath);
  //  $apiWorker->InsertNewAddress(42, "New Institution", "Víťovat", "77855", "Morava", 48);
  //  $apiWorker->UpdateSentPostcards(1, "FSS muni", 10, "Víťa Kratochvíl");
//End of - Examples of use
?>
